package medicine_ledger

import (
	"database/sql"
	"strconv"
	"time"
)

type MedicineLedgerDB struct {
	db *sql.DB
}

type ResultStatus struct {
	ID    string
	Error error
}

type ResultStatuses map[string]*ResultStatus

func Open(db *sql.DB) (*MedicineLedgerDB, error) {
	var medLedgerDB MedicineLedgerDB
	medLedgerDB.db = db
	return &medLedgerDB, nil
}

func (this *MedicineLedgerDB) GetTransactions() (Transactions, error) {
	rows, err := this.db.Query("SELECT transaction_id, medicine_id, amount, transaction_date FROM medicine_ledger;")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	medicines := make(Transactions)

	for rows.Next() {
		var (
			id         int
			medicineID int
			amount     float32
			date       time.Time
		)

		err = rows.Scan(&id, &medicineID, &amount, &date)
		if err != nil {
			return nil, err
		}
		medicine := &Transaction{
			id,
			medicineID,
			amount,
			date,
		}

		medicines[id] = medicine
	}
	return medicines, nil
}

func (this *MedicineLedgerDB) GetMedTransactions(medID int) (Transactions, error) {
	rows, err := this.db.Query("SELECT transaction_id, medicine_id, amount, transaction_date FROM medicine_ledger WHERE medicine_id = ?;", medID)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	transactions := make(Transactions)

	for rows.Next() {
		var (
			id         int
			medicineID int
			amount     float32
			date       time.Time
		)

		err = rows.Scan(&id, &medicineID, &amount, &date)
		if err != nil {
			return nil, err
		}
		transaction := &Transaction{
			id,
			medicineID,
			amount,
			date,
		}

		transactions[id] = transaction
	}
	return transactions, nil
}

func (this *MedicineLedgerDB) GetTransaction(id int) (*Transaction, error) {
	rows, err := this.db.Query("SELECT transaction_id, medicine_id, amount, transaction_date FROM medicine_ledger WHERE transaction_id = ?;", id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var transaction *Transaction

	for rows.Next() {
		var (
			id         int
			medicineID int
			amount     float32
			date       time.Time
		)
		err = rows.Scan(&id, &medicineID, &amount, &date)
		if err != nil {
			return nil, err
		}

		transaction = &Transaction{
			id,
			medicineID,
			amount,
			date,
		}
		break
	}
	return transaction, nil
}

func (this *MedicineLedgerDB) CUTransactions(transactions []Transaction) ResultStatuses {
	results := make(ResultStatuses)
	newCount := 0
	for _, transaction := range transactions {
		if transaction.ID <= 0 {
			var result *ResultStatus
			var keyID string
			newID, err := this.CreateTransaction(&transaction)
			if err != nil {
				keyID = "new-" + strconv.Itoa(newCount)
				result = &ResultStatus{
					keyID,
					err,
				}
				newCount = newCount + 1
			} else {
				keyID = strconv.Itoa(newID)
				result = &ResultStatus{
					keyID,
					nil,
				}
			}
			results[keyID] = result
		} else {
			err := this.UpdateTransaction(&transaction)
			if err != nil {
				result := &ResultStatus{
					strconv.Itoa(transaction.ID),
					err,
				}
				results[strconv.Itoa(transaction.ID)] = result
			} else {
				result := &ResultStatus{
					strconv.Itoa(transaction.ID),
					nil,
				}
				results[strconv.Itoa(transaction.ID)] = result
			}
		}
	}
	return results
}

func (this *MedicineLedgerDB) CreateTransaction(transaction *Transaction) (int, error) {
	result, err := this.db.Exec("INSERT INTO medicine_ledger (medicine_id, amount) VALUES (?, ?);", transaction.MedicineID, transaction.Amount)
	if err != nil {
		return 0, err
	}
	returnID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(returnID), nil
}

func (this *MedicineLedgerDB) UpdateTransaction(transaction *Transaction) error {
	_, err := this.db.Exec("UPDATE medicine_ledger SET medicine_id = ?, amount = ? WHERE transaction_id = ?;", transaction.MedicineID, transaction.Amount, transaction.ID)
	if err != nil {
		return err
	}
	return nil
}

func (this *MedicineLedgerDB) DeleteTransaction(id int) error {
	_, err := this.db.Exec("DELETE FROM medicine_ledger WHERE transaction_id = ?;", id)
	if err != nil {
		return err
	}
	return nil
}

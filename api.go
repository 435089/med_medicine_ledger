package medicine_ledger

import "time"

type Transaction struct {
	ID         int       `json:"id"`
	MedicineID int       `json:"medicine_id"`
	Amount     float32   `json:"amount"`
	Date       time.Time `json:"date"`
}

type Transactions map[int]*Transaction

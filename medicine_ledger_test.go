package medicine_ledger

import (
	"database/sql"
	"fmt"
	"strconv"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func TestGetTransactions(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}
	transactions, err := transDB.GetTransactions()
	if err != nil {
		t.Error(err)
	}
	if transactions == nil {
		t.Log("No transactions returned from the GetTransactions() method call")
		t.Fail()
	}
}

func TestGetMedTransactions(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	tempTrans, err := GetFirstAvailableTransaction(transDB)
	if err != nil {
		t.Error(err)
	}

	transactions, err := transDB.GetMedTransactions(tempTrans.MedicineID)
	if err != nil {
		t.Error(err)
	}
	if transactions == nil {
		t.Log("No transactions returned from the GetTransactions() method call")
		t.Fail()
	}

	for _, value := range transactions {
		if value.MedicineID != tempTrans.MedicineID {
			t.Log("Values retrieved from query have mismatching Medicine IDs")
			t.Fail()
		}
	}
}

func TestGetTransaction(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	transID := 0

	tempTrans, err := GetFirstAvailableTransaction(transDB)
	if err != nil {
		t.Error(err)
	}

	transID = tempTrans.ID

	if transID > 0 {
		transaction, err := transDB.GetTransaction(transID)
		if err != nil {
			t.Error(err)
		}

		if transaction == nil {
			t.Log("Transaction result is nil")
			t.Fail()
		}
		if transaction.ID != transID {

			t.Logf("Transaction id is not equal to what was provided. expected: %v, got: %v", transID, transaction.ID)
			t.Fail()
		}
	} else {
		t.Log("Unable to run GetTransaction test. No records exist.")
		t.Fail()
	}
}

func TestCreateTransaction(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	tempTrans, err := GetFirstAvailableTransaction(transDB)
	if err != nil {
		t.Error(err)
	}

	transaction := &Transaction{
		0,
		tempTrans.MedicineID,
		50,
		time.Now(),
	}

	newID, err := transDB.CreateTransaction(transaction)
	if err != nil {
		t.Error(err)
	}

	newTransaction, err := transDB.GetTransaction(newID)
	if err != nil {
		t.Error(err)
	}

	if newTransaction == nil {
		t.Log("Transaction insert did not return the correct transaction id")
		t.Fail()
	}

	if newTransaction.ID != newID {
		t.Log("Transaction ID is not the same")
		t.Fail()
	}

	if newTransaction.Amount != transaction.Amount {
		t.Log("Transaction Amount is not the same")
		t.Fail()
	}

	if newTransaction.MedicineID != transaction.MedicineID {
		t.Log("Transaction Medicine ID is not the same")
		t.Fail()
	}
}

func TestUpdateTransaction(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	transaction, err := GetFirstAvailableTransaction(transDB)
	if err != nil {
		t.Error(err)
	}

	newAmount := float32(-10)

	transaction.Amount = newAmount

	err = transDB.UpdateTransaction(transaction)
	if err != nil {
		t.Error(err)
	}

	newTransaction, err := transDB.GetTransaction(transaction.ID)
	if err != nil {
		t.Error(err)
	}

	if newTransaction.ID != transaction.ID {
		t.Log("Transaction ID has changed when it was not supposed to")
		t.Fail()
	}

	if newTransaction.Amount != transaction.Amount {
		t.Log("Transaction Amount has changed when it was not supposed to")
		t.Fail()
	}
}

func TestCUMedicines(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	transactions := make([]Transaction, 2)

	transaction2, err := GetFirstAvailableTransaction(transDB)
	if err != nil {
		t.Error(err)
	}

	transaction2.Amount = float32(20)

	transaction1 := Transaction{
		0,
		transaction2.MedicineID,
		30,
		time.Now(),
	}

	transactions[0] = transaction1
	transactions[1] = *transaction2

	results := transDB.CUTransactions(transactions)

	for _, value := range results {
		if value.Error != nil {
			t.Error(value.Error)
		} else {
			id, err := strconv.Atoi(value.ID)
			if err != nil {
				t.Error(err)
			}
			if id <= 0 {
				t.Log("NewID is less than or equal to zero")
				t.Fail()
			}
		}
	}
}

func TestDeleteMedicine(t *testing.T) {
	db, err := sql.Open("mysql", "medical:gTRMXZmAMAtebRtL2HWXlAXHzgVEWAk36zgUFoYRay0yOcV65jm4MLSGcCR32a1@(localhost:3306)/CR_Medical?parseTime=true")
	if err != nil {
		t.Error(err)
	}

	transDB, err := Open(db)
	if err != nil {
		t.Error(err)
	}

	tempTrans, err := GetFirstAvailableTransaction(transDB)
	if err != nil {
		t.Error(err)
	}

	id := tempTrans.ID

	transDB.DeleteTransaction(id)

	transaction, err := transDB.GetTransaction(id)
	if err != nil {
		t.Error(err)
	}
	if transaction != nil {
		t.Log("Transaction delete failed")
		t.Fail()
	}
}

func GetFirstAvailableTransaction(transDB *MedicineLedgerDB) (*Transaction, error) {
	transList, err := transDB.GetTransactions()
	if err != nil {
		return nil, err
	}

	if len(transList) <= 0 {
		return nil, fmt.Errorf("No available medicines to return")
	}

	var transaction *Transaction

	for _, value := range transList {
		transaction = value
		break
	}
	return transaction, nil
}
